﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _0502MVC.Models
{

    [MetadataType(typeof(UsersMetaData))]
    public partial class Users
    {

    }

    public class UsersMetaData
    {
        [Required(ErrorMessage ="必填欄位")]
        [DisplayName("Name")]
        [StringLength(8)]
        public string Name { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("Passward")]
        [StringLength(16)]
        public string Passward { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("Birthday")]
        public System.DateTime Birthday { get; set; }
                
        public bool Gender { get; set; }
    }
}