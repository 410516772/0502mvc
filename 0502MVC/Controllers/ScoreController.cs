﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _0502MVC.Controllers
{
    public class ScoreController : Controller
    {
        // GET: Score
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(int score)
        {
            string code = "";

            if (score <=100 && score >=80)
            {
                code = "A";
            }
            else if (score <=79 && score >=60)
            {
                code = "B";
            }
            else if (score <= 59 && score >= 40)
            {
                code = "C";
            }
            else if (score <= 39 && score >=20 )
            {
                code = "D";
            }
            else if (score <= 19 && score >= 0)
            {
                code = "E";
            }
            ViewBag.score = score;
            ViewBag.code = code;
            return View();
        }
    }
}