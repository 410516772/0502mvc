﻿using _0502MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _0502MVC.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult Index()
        {
            return View(new BMIData());
        }

        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            // if (data.Weight <= 0 || data.Weight > 200) {
            //     ViewBag.weighterr=("請輸入1~200的數字");
            // }
            // if (data.Height < 80 || data.Height > 300)
            // {
            //     ViewBag.heighterr=("請輸入80~300的數字");
            // }
            if (ModelState.IsValid)
            {
                float m_height = data.Height / 100;
                float BMI = data.Weight / (m_height * m_height);
                var level = "";
                if (BMI < 18.5)
                {
                    level = "體重過輕";
                }
                else if (18.5 <= BMI && BMI < 24)
                {
                    level = "正常範圍";
                }
                else if (24 <= BMI && BMI < 27)
                {
                    level = "過重";
                }
                else if (27 <= BMI && BMI < 30)
                {
                    level = "輕度肥胖";
                }
                else if (30 <= BMI && BMI < 35)
                {
                    level = "中度肥胖";
                }
                else if (35 <= BMI)
                {
                    level = "重度肥胖";
                }
                data.BMI = BMI;
                data.Level = level;
            }
            return View(data);
        }
    }
}