﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _0502MVC.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Login()  {
            TempData["msg"] = "Hello world";
            ViewData["msg"] = "Hello world";
            ViewBag.msg = "Hello world";
            return RedirectToAction("Index");
        }
        public ActionResult Index()
        {
            ViewData["msg"] = "Hello world";
            ViewBag.msg = "Hello world";
            return View();
        }
    }
}