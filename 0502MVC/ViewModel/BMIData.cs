﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _0502MVC.ViewModel
{
    public class BMIData
    {
        [Required(ErrorMessage = "必填")]
        [Range(30,150,ErrorMessage = "請填入30~150")]
        [Display(Name ="體重")]
        public float Weight { get; set; }
        
        [Required(ErrorMessage = "必填")]
        [Range(50, 250, ErrorMessage = "請填入50~250")]
        [Display(Name = "身高")]
        public float Height { get; set; }
        public float BMI { get; set; }
        public string Level { get; set; }
    }
}