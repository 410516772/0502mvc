﻿CREATE TABLE [dbo].[Users]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(10) NOT NULL, 
    [Email] NVARCHAR(50) NOT NULL, 
    [Passward] NVARCHAR(10) NOT NULL, 
    [Birthday] DATETIME2 NOT NULL, 
    [Gender] BIT NOT NULL
)
